#include <iostream>

#include "Ch1.h"
#include "Ch2.h"
#include "Ch3.h"

int main()
{
	std::cout << "Chapter 1 TEST:\n";
	Ch1::test();
	std::cout << "Chapter 2 TEST:\n";
	Ch2::test();
	std::cout << "Chapter 3 TEST:\n";
	Ch3::test();
	return 0;
}
