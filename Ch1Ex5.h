#pragma once

#include<iostream>

//Chapter 1 ex 5

template<typename ... Targs>
class Character : public Targs ...
{
public:
	Character(int hp, Targs ... args) : Targs(args) ..., _hp(hp)
	{

	}

	void present()
	{
		std::cout << "Character with HP: " << _hp << std::endl;
		if constexpr (sizeof...(Targs) > 0)
		{
			std::cout << "Skills avaliable: " << std::endl;
			(Targs::action(), ...);
		}
	}

private:
	int _hp;
};

class Fighter
{
public:
	Fighter(int str) : _strength(str)
	{
	}

	void action()
	{
		std::cout << "Sword attack: " << _strength << std::endl;
	}
private:
	int _strength;
};

class Archer
{
public:
	Archer(int agi) : _agility(agi)
	{
	}

	void action()
	{
		std::cout << "Bow attack: " << _agility << std::endl;
	}
private:
	int _agility;
};

class Mage
{
public:
	Mage(int man, int pow) : _mana(man), _power(pow)
	{
	}

	void action()
	{
		std::cout << "Magic attack: " << _power << ":" << _mana << std::endl;
	}
private:
	int _mana;
	int _power;
};