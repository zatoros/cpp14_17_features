#pragma once

#include<iostream>
#include<string>

//Chapter 1 ex 8

template<char ... args>
class Tailor
{
public:
	static const int TAIL_SIZE = 15;

	Tailor() : tailSize(strlen(tailData))
	{

	}

	void tailor()
	{
		for (int i = 0; i < 15; i++)
			printLine(i % tailSize);
	}
private:
	void printNormal(int offset)
	{
		for (size_t i = offset; i < tailSize; i++)
			std::cout << tailData[i];
		for (size_t i = 0; i < offset; i++)
			std::cout << tailData[i];
	}

	void printReverse(int offset)
	{
		for (int i = offset - 1; i >= 0; i--)
			std::cout << tailData[i];
		for (int i = tailSize - 1; i >= offset; i--)
			std::cout << tailData[i];
	}

	void printLine(int offset)
	{
		printNormal(offset);
		printReverse(offset);
		printNormal(offset);
		printReverse(offset);
		std::cout << std::endl;
	}

	static constexpr char tailData[sizeof...(args) + 1] = { args ... };
	const size_t tailSize;
};