#pragma once

#include<algorithm>
#include<bitset>
#include<iostream>
#include<string>
#include<type_traits>
#include<vector>

template<typename T>
void printBinary(T number, std::vector<int> separators = {})
{
	static_assert(std::is_constructible<std::bitset<sizeof(T) * 8>, T>::value,
		"Number type needs to be numeric");

	if (number == 0)
	{
		std::cout << "0\n";
		return;
	}

	std::bitset<sizeof(T) * 8> bitnumber(number);
	bool firstPrint = false;
	for (int bitIndex = bitnumber.size() - 1; bitIndex >= 0; --bitIndex)
	{
		if (!firstPrint && !bitnumber[bitIndex])
			continue;
		firstPrint = true;

		std::cout << bitnumber[bitIndex];
		if (std::find(separators.begin(), separators.end(), bitIndex) != separators.end())
		{
			std::cout << "`";
		}
	}
	std::cout << std::endl;
}

unsigned long loadBinary()
{
	std::string data;
	getline(std::cin, data);
	data.erase(std::remove(data.begin(), data.end(),'`'), data.end());
	if (data[0] == '0' && (data[1] == 'b' || data[1] == 'B'))
	{
		data.erase(0, 2);
	}
	unsigned long result = std::bitset<sizeof(unsigned long) * 8>(data).to_ulong();
	std::cout << result << std::endl;
	return result;
}