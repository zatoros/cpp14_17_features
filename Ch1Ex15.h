#pragma once

#include<iostream>

template<typename ... Targs>
auto sum(Targs ... args)
{
	return (... + args);
}

template<typename ... Targs>
void show(Targs ... args)
{
	((std::cout << args << " "), ...);
	std::cout << std::endl;
}

template<typename ... Targs>
void workshop(Targs ... args)
{
	std::cout << "show(args ...): ";
	show(args ...);
	std::cout << "show(2 * args ...): ";
	show(2* args ...);
	std::cout << "show(args + 5 ...): ";
	show(args + 5 ...);
	std::cout << "show(args * args ...): ";
	show(args * args ...);
	std::cout << "show(args ..., args ...): ";
	show(args ..., args ...);
	std::cout << "show(sum(args ...) + args ...): ";
	show(sum(args ...) + args ...);
}