#pragma once

#include <iostream>
#include <string>
#include <vector>

//Chapter 1 ex 3

namespace
{
	inline std::ostream& operator<<(std::ostream& out, const std::vector<double>& vec)
	{
		for (const auto& el : vec)
			out << el << " ";
		return out;
	}
}


class ch1ex3
{
public:
	ch1ex3() = default;

	template<typename ... Targs>
	void collectData(Targs ... args)
	{
		static_assert(
			(std::is_convertible<Targs, double>::value && ...),
			"collectData arguments need to be convertible to double");
		if constexpr (sizeof...(args) == 0)
			return;
		((args < 10 ? loVec.push_back(args) :
			args < 100 ? midVec.push_back(args) :
			hiVec.push_back(args)),
			...);
	}

	void printVectors()
	{
		printVec("hiVec", hiVec);
		printVec("miVec", midVec);
		printVec("loVec", loVec);
	}

private:
	void printVec(const std::string& header, const std::vector<double> data)
	{
		std::cout << header << ": " << data << std::endl;
	}

	std::vector<double> hiVec;
	std::vector<double> midVec;
	std::vector<double> loVec;
};