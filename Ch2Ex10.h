#pragma once

#include<iostream>

void runGenericLambda()
{
	auto func = [](auto number, int bit) -> bool 
	{ 
		return (number & static_cast<decltype(number)>(1) << (bit - 1)) > 0; 
	};
	std::cout << func(static_cast<unsigned>(0b111), 1) << std::endl;
	std::cout << func(static_cast<unsigned short>(0b111), 2) << std::endl;
	std::cout << func(static_cast<unsigned long>(0b111), 3) << std::endl;
	std::cout << func(static_cast<unsigned long long>(0b111), 4) << std::endl;
}