#pragma once

#include <iostream>

struct alignas(32) A
{
	int a;
	int b;
	int c;
};

struct alignas(64) B
{
	int a;
	int b;
	int c;
};

struct C
{
	int a;
	int b;
	int c;
};

void alignTest()
{
	alignas(128) C d;
	std::cout << "alignof(A): " << alignof(A) << std::endl;
	std::cout << "alignof(B): " << alignof(B) << std::endl;
	std::cout << "alignof(C): " << alignof(C) << std::endl;
	std::cout << "alignof(d): " << alignof(decltype(d)) << std::endl;
}