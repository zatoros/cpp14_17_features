#pragma once

#include <iostream>

template<typename ... Targs>
void IncrementRef(Targs & ... args)
{
	(++args, ...);
}

template<typename ... Targs>
void IncrementPtr(Targs * ... args)
{
	(++(*args), ...);
}

template<typename ... Targs>
void IncrementConstPtr(Targs * const ... args)
{
	(++(*args), ...);
}

template<typename ... Targs>
void DisplayRef(const Targs & ... args)
{
	(std::cout << ... << args) << std::endl;
}

template<typename ... Targs>
void DisplayPtr(const Targs * const ... args)
{
	(std::cout << ... << *args) << std::endl;
}