#include "Ch2.h"
#include "Ch2ex1_4.h"
#include "Ch2ex7.h"
#include "Ch2Ex10.h"
#include "Ch2ex15_25.h"

namespace Ch2
{
	void test()
	{
		printBinary(0b0011);
		printBinary(0b001100);
		printBinary(0b00110000);
		printBinary(0b00);
		printBinary(0b10101010101010, { 3,5,6 });
		loadBinary();
		fa();
		fb();
		fc();
		fd();
		fe();
		ff();
		fg();
		fh();
		runGenericLambda();
		//Dep d;
		//dep;
	}
}