#pragma once

//noexcept(false) is deafult
//noexcept(true) == noexcept

void func1([[maybe_unused]] int a, [[maybe_unused]] char b) noexcept
{

}

void func2([[maybe_unused]] int a, [[maybe_unused]] char b) noexcept(true)
{

}

void func3([[maybe_unused]] int a, [[maybe_unused]] char b) noexcept(false)
{

}

void func4([[maybe_unused]] int a, [[maybe_unused]] char b)
{

}

void testNoExcept()
{
	void(*funcPtr1)(int, char) noexcept(true);
	void(*funcPtr2)(int, char) noexcept(false);
	void(*funcPtr3)(int, char);

	funcPtr1 = func1;
	funcPtr1 = func2;
	//funcPtr1 = func3;
	//funcPtr1 = func4;

	funcPtr2 = func1;
	funcPtr2 = func2;
	funcPtr2 = func3;
	funcPtr2 = func4;

	funcPtr3 = func1;
	funcPtr3 = func2;
	funcPtr3 = func3;
	funcPtr3 = func4;
}