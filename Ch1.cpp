#include "Ch1Ex3.h"
#include "Ch1Ex5.h"
#include "Ch1Ex8.h"
#include "Ch1Ex9_13.h"
#include "Ch1Ex15.h"

namespace Ch1
{
	void test()
	{
		ch1ex3 test1;
		test1.collectData(1, 2, 3, 10, 1, 2, 3, 15, 18, 20, 456, 1, 234, 2555, 88);
		test1.printVectors();
		//test1.collectData(1, 2, 3, "a"); // static assert test
		test1.collectData();

		Character<Fighter, Mage>(50, Fighter(1), Mage(1, 2)).present();
		Character<Fighter, Archer>(25, Fighter(4), Archer(8)).present();
		Character<Fighter, Archer, Mage>(25, Fighter(4), Archer(8), Mage(5, 10)).present();
		Character<>(125).present();

		Tailor<' ','.','.','/','\\','O','o', 'o', 'o','.','/','\\','/','\\'>().tailor();

		int a = 1, b = 2, c = 3, d = 4;
		DisplayRef(a, b, c, d);
		IncrementRef(a, b, c, d);
		DisplayRef(a, b, c, d);
		IncrementPtr(&a, &b, &c, &d);
		DisplayPtr(&a, &b, &c, &d);
		IncrementConstPtr(&a, &b, &c, &d);
		DisplayPtr(&a, &b, &c, &d);

		workshop(1, 2.0, 3);
	}
}