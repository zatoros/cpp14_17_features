#pragma once

#include <iostream>
#include <string>

//Could not find macro which will return actuall return type :(

auto fa()
{
	std::cout << __FUNCSIG__ << std::endl;
	return 'a';
}

auto fb()
{
	std::cout << __FUNCSIG__ << std::endl;
	return 3.14;
}

auto fc()
{
	std::cout << __FUNCSIG__ << std::endl;
	return 4u;
}

auto fd()
{
	std::cout << __FUNCSIG__ << std::endl;
	return 1u + 5.1;
}

auto fe()
{
	std::cout << __FUNCSIG__ << std::endl;
	return (int) 4.1 + 2.2;
}

auto ff()
{
	std::cout << __FUNCSIG__ << std::endl;
	return static_cast<char>(3.5);
}

auto fg()
{
	std::cout << __FUNCSIG__ << std::endl;
	return "hej";
}

auto fh()
{
	std::cout << __FUNCSIG__ << std::endl;
	return "raz" + std::string("dwa");
}