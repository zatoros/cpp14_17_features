#pragma once

//Attributes test

[[ noreturn ]] void noReturnFcn()
{
	throw "Exception";
}

class[[deprecated("AAA")]] Dep
{

};

[[deprecated("BBB")]] class Dep2
{

} dep;
